// Generics is a type data that accept one parameter
// là truyền type vào components, có thể tái sử dụng tốt hơn
// nhan du lieu va tra lai du lieu tuong ung
function getType<T>(a: T, b: T): [T, T] {
  return [a, b];
}

const getType1 = <T>(a: T, b: T): [T, T] => {
  // const getTuple: <T>(a: T, b: T) => [T,T] { : it is the same with above
  return [a, b];
};

let stringArray = getType<string>("Hellow", "World");
let numberArray = getType<number>(113, 123);

let ucStrings = stringArray.map((s) => s.toUpperCase());

console.log(ucStrings);
// ======================================================
// declare generic function using interface

interface TypeFunction {
  <T, U>(a: T, b: U): [T, U];
}

var getType2: TypeFunction = (a, b) => {
  return [a, b];
};

let stringArray1 = getType2<string, string>("Hello", "World");
