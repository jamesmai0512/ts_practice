var contact = [];
var newContact = {
    name: "James Mai",
    phone: "0907270925",
    email: "jamesmai.sg@gmail.com"
};
console.log(newContact);
// Optional Properties (?) properties là thuoc tính
// là có cũng được hoặc không có cũng được
// Email có cũng được hoặc không có cũng được
// line 6 email
// ========================================================
// Class
// Define a class in typescript
var Student = /** @class */ (function () {
    function Student(n, a) {
        this.name = n;
        this.age = a;
    }
    Student.prototype.intro = function () {
        return "The student name is ".concat(this.name, " and student age is ").concat(this.age);
    };
    return Student;
}());
var newStudent = new Student("James Mai", 18);
console.log(newStudent.intro());
