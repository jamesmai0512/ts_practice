// Interface
// Interface là dùng để design cái hình thù của một Object, và data type nào đó
interface Contact {
  name: string;
  phone: string;
  email?: string; //optionalf
}

const contact: Contact[] = [];

const newContact: Contact = {
  name: "James Mai",
  phone: "0907270925",
  email: "jamesmai.sg@gmail.com",
};

console.log(newContact);
