export {};
// Optional Properties (?) properties là thuoc tính
// là có cũng được hoặc không có cũng được
// Email có cũng được hoặc không có cũng được
// line 6 email

// ========================================================

// Class
// Define a class in typescript

class Student {
  // Define properties of student
  name: string;
  age: number;
  constructor(n: string, a: number) {
    this.name = n;
    this.age = a;
  }

  intro() {
    return `The student name is ${this.name} and student age is ${this.age}`;
  }
}

const newStudent = new Student("James Mai", 18);
console.log(newStudent.intro());

// ========================================================
// Function
let greet: Function;

greet = () => {
  console.log("Hello James");
};

// Function not gonna return any thing
const add = (a: number, b: number, c: number | string = 20): void => {
  console.log(a + b);
  console.log(c);
};

add(2, 2, 3);

// Function returning somthing
const minus = (a: number, b: number): number => {
  return a + b;
};

let result = minus(10, 7);
console.log(result);

// Enum enumerable
// là một cái dạng liệt kê các giá trị có thể cho kiểu dữ liệu đó
enum StudentType {
  Bad,
  Medium,
  Good,
}
interface Student {
  name: string;
  age: number;
  type: StudentType;
}

const newStudent1: Student = {
  name: "James",
  age: 19,
  type: StudentType.Good,
};
console.log(newStudent1);

// ======================================

interface Class {
  class: number;
  name: string;
}

const numberList: Array<number> = [1, 22, 4, 5];

const newClass: Array<Class> = [
  { class: 1, name: "Nguyen Hue" },
  { class: 2, name: "PCT" },
];

// Decorators: trang trí
// Before the decorator it has a @
// @expresstion also is a function

function AutoBind(target: any, key: string, description: PropertyDescriptor);
class Printer {
  message = "This works";

  showMessage() {
    console.log(this.message);
  }
}

const p = new Printer();
p.showMessage();

////

// In Module it has: Import / Export
/////////////

import logger from "...";
