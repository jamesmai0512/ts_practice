"use strict";
exports.__esModule = true;
// Boolean
var isDone = false;
// Number
var age = 20;
// String
var color = "blue";
console.log(color);
// Array
var array = [1, 2, 3, 4, 5, 6];
console.log(array);
// Tuple
// Là sẽ đặt kiểu dữ liệu mong muốn vào trong mảng và khi sử dụng sẽ phải tuân theo kiểu dử liệu đó
var person = [18, "James", true];
// Tuple Array
var car;
car = [
    [1, "Lambo"],
    [2, "Ferrari"],
];
// Enum
// Enum là một cách đặt tên với những giá trị số
var Color;
(function (Color) {
    Color[Color["red"] = 1] = "red";
    Color[Color["green"] = 2] = "green";
    Color[Color["blue"] = 3] = "blue";
})(Color || (Color = {}));
var colorName = Color[2];
// let colorName: number = Color.blue;
console.log(colorName);
// Any
// any là kiểu dử liệu gì cũng được
var notSure = 4;
notSure = "Hihi";
notSure = true;
var arrayAny = [1, "this", 123];
console.log(arrayAny);
arrayAny[1] = 100;
console.log(arrayAny);
console.log(arrayAny);
// Unknown
// Meaning return the unknown
// Void
// Void wont return any value so you can only assign two values are null and undefined
// Never
// Object
