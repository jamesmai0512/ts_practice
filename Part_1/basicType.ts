export {};
// Boolean
let isDone: boolean = false;

// Number
let age: number = 20;

// String
let color: string = "blue";
console.log(color);

// Array
// array of something you have to write the type before the array
// Ex: string[]
let array: number[] = [1, 2, 3, 4, 5, 6];
console.log(array);

// Tuple
// Là sẽ đặt kiểu dữ liệu mong muốn vào trong mảng và khi sử dụng sẽ phải tuân theo kiểu dử liệu đó
// tuple giong array cho minh define khac kieu du lieu trong mang
const person: [number, string, boolean] = [18, "James", true];
// Tuple Array
let car: [number, string][];
car = [
  [1, "Lambo"],
  [2, "Ferrari"],
];

// Enum
// Enum là một cách đặt tên với những giá trị số
enum Color {
  red = 1,
  green,
  blue,
}

let colorName: string = Color[2];
// let colorName: number = Color.blue;
console.log(colorName);

// Any
// any là kiểu dử liệu gì cũng được
let notSure: any = 4;
notSure = "Hihi";
notSure = true;

let arrayAny: any[] = [1, "this", 123];
console.log(arrayAny);
arrayAny[1] = 100;
console.log(arrayAny);

// Unknown
// Meaning return the unknown

// Void
// A function in typescript don't actually
// return something just return the void
// Void wont return any value so you can only assign two values are null and undefined

// Never

// Object
